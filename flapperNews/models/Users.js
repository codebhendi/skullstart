var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
	local            : {
    email        : String,
    name         : String,
    userUrl      : String,
   	salt         : String,
   	hash         : String
  },
  facebook         : {
    id           : String,
    token        : String,
    email        : String,
    name         : String
  },
  twitter          : {
    id           : String,
    token        : String,
    displayName  : String,
    username     : String
  },
  google           : {
    id           : String,
    token        : String,
    email        : String,
    name         : String
  },
  date             : Date
});

userSchema.methods.setPassword = function (password) {
	this.local.salt = crypto.randomBytes(16).toString('hex');

	this.local.hash = crypto.pbkdf2Sync(password, this.local.salt, 1000, 64).toString('hex');
}

userSchema.methods.validPassword = function (password) {
	var hash = crypto.pbkdf2Sync(password, this.local.salt, 1000, 64).toString('hex');

	return hash === this.local.hash;
}

userSchema.methods.generateJWT = function() {
	var today = new Date();
	var exp = new Date(today);
	exp.setDate(today.getDate() + 60)

	return jwt.sign({
		_id		  : this._id,
		email   : this.local.email,
		name 	  : this.local.name,
    userUrl : this.local.userUrl,
		exp: parseInt(exp.getTime() / 1000),
	}, 'SECRET');
}

mongoose.model('User', userSchema);