var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var configAuth = require('./auth');

passport.use('local', new localStrategy(
	function(email, password, done) {
		User.findOne({'local.email': email}, function (err, user) {
			if (err) {
				return done(err);
			}

			if (!user) {
				return done(null, false,  {message: "Incorrect Email"});
			}

			if (!user.validPassword(password)) {
				return done(null, false, {message: "Incorrect password"});
			}

			return done(null, user);
		});
	}
));

passport.use('local-signup', new localStrategy({
		usernameField : 'email',
	  passwordField : 'password',
	  passReqToCallback : true
	}, 
  function(req, email, password, done) {
	  
	  // User.findOne wont fire unless data is sent back
	  process.nextTick(function() {

	    // find a user whose username is the same as the forms email
	    // we are checking to see if the user trying to login already exists
	    User.findOne({'local.email' :  email}, function(err, user) {
        // if there are any errors, return the error
        if (err) {
          return done(err);
        }
        // check to see if theres already a user with that email
        if (user) {
          return done(null, false, {message: 'That email'+ 
          	'is already taken.'});
        } else {

          // if there is no user with that email
          // create the user
          var user = new User();

          // set the user's local credentials
          user.local.name = req.body.name;
          var userUrl = user.local.name;
          userUrl = userUrl.replace(/ /g, '-')
          userUrl = userUrl.toLowerCase();
          user.local.email = email;
          user.setPassword(password);
          user.date = new Date();

          User.find({"local.name": user.name}, function(err, users) {
            if (err) {
              return done(err);
            }

            if (user[0] != undefined) {
              var index = users[users.length - 1];
              index = parseInr(index[index.length - 1]);
              if (typeOf(index)!== "number") {
                index = '1';
              } else {
                index++;
              }
              userUrl += '-' + index;
            }
            user.local.userUrl = userUrl;
            user.save(function(err, one) {
                  if (err) {
                      throw err;
                  }
                  console.log(one);
                  return done(null, user);
              });

            });
        }

	    });    

	  });

  })
);